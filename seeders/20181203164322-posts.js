'use strict';
const models = require('../models');
var randomSentences = require('random-sentence');

module.exports = {
  up: (queryInterface, Sequelize) => {

    return models.User.findAll({ attributes: ['id'] }).then(users => {
      // projects will be an array of all Project instances
      var posts = [];
      users.forEach(function (user){
        const i = 200+user.id
        const picture = `https://picsum.photos/${i}`

        posts.push({
          title: randomSentences(),
          description: randomSentences(),
          img: picture,
          userId: user.id,
          createdAt: new Date(),
          updatedAt: new Date()
        })
      })

      return queryInterface.bulkInsert('Posts', posts, {});
    })
    


    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Posts', null, {});
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
