const models = require('../models');
const bcrypt = require('bcrypt');
var jwt  = require('jsonwebtoken')
require('dotenv').config()
/**
 * Classe d'authentification d'un utilisateur
 */
class AuthentificationController{
    
    /**
     * Création d'un user
     * @param {ContentUser} req 
     * @param {ResponseSended} res 
     */
    static register(req,res) {

        const pseudo = req.body.pseudo;
        const mail = req.body.mail;
        const password = req.body.password;
        const createdAt = new Date();
        const updatedAt = new Date();

        var hashedPassword = bcrypt.hashSync(password, 12);

        models.User.create({
            pseudo,
            mail,
            password: hashedPassword,
            createdAt,
            updatedAt
        })
        .then(e =>{
            res.status(200).send({message: "Profil bien crée"});
        })

    }

    /**
     * Connexion d'un utilisateur
     * @param {ContentUser} req 
     * @param {ResponseSended} res 
     */
    static login(req,res) {

        const pseudo = req.body.pseudo;
        const password = req.body.password;

        models.User.findAll({
            where: {
              pseudo,
            }
          })
          .then(data => {
            let user = data[0];
            bcrypt.compare(password, user.password, (err,rese) =>{
                if(rese){

                    const payload = { id : user.id }
                    const expiresIn = 4*60*60 // 4h
                    const token = jwt.sign(payload, process.env.SECRET_TOKEN, { expiresIn: expiresIn })
                    res.cookie("access_token",token);
                    res.status(200).send({ 
                        message: "Sent"
                    }); 
                }
                else{
                    res.status(403).send({ 
                        message: "Mauvais logs"
                    });
                }
            })
          });
    }

    /**
     * Connexion d'un utilisateur
     * @param {ContentUser} req 
     * @param {ResponseSended} res 
     */
    static logout(req,res){

        res.clearCookie('access_token');
        res.status(200).send({ 
            message: "Vous avez été deconnecté"
        });
    }
}

module.exports = AuthentificationController;