var models = require('../models')
/**
 * Classe d'un utilisateur
 */
class UserController{

    /**
     * Récupération d'un user
     * @param {ContentUser} req 
     * @param {ResponseSended} res 
     */
    static getById(req,res) {
        var id = req.params.id;
        
        models.User.findById(id).then(user => {
            res.status(200).send({ user });
          })

    }
}

module.exports = UserController;