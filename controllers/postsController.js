var models = require('../models')
/**
 * Classe d'un Post
 */
class PostsController{

    /**
     * Récupération de tous les posts
     * @param {ContentPost} req 
     * @param {ResponseSended} res 
     */
    static getAll(req,res) {
        models.User.findAll({
            include: [{
                model: models.Posts,
            }],
        }).then(posts => {
            res.status(200).send({ posts });    
        })
    }

    /**
     * Récupération d'un post
     * @param {ContentPost} req 
     * @param {ResponseSended} res 
     */
    static getById(req,res) {
        var id = req.params.id;
        
        models.Posts.findById(id).then(post => {
            res.status(200).send({ post });
          })
    }

    /**
     * Création d'un post
     * @param {ContentPost} req 
     * @param {ResponseSended} res 
     */
    static newOne(req,res) {

        const title = req.body.title;
        const img = req.body.img;
        const description = req.body.description;
        const UserId = req.body.userId
        const createdAt = new Date();
        const updatedAt = new Date();

        models.Posts.create({
            title,
            img,
            description,
            UserId,
            createdAt,
            updatedAt
        })
        .then(e =>{
            res.status(200).send({message: "Post bien crée"});
        })
    }

    /**
     * Mise à jour d'un post
     * @param {ContentPost} req 
     * @param {ResponseSended} res 
     */
    static update(req,res) {
        res.status(200).send({ 
            endpoint: "PostsController.update" 
        });
    }

    /**
     * Suppression d'un post
     * @param {ContentPost} req 
     * @param {ResponseSended} res 
     */
    static delete(req,res) {
        var id = req.params.id;
        
        models.Posts.destroy({
            where: {
                id
            }
        })
        .then(post => {
            res.status(200).send({ message:"supprimer" });
        })
    }
}

module.exports = PostsController;