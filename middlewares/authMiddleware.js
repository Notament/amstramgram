var jwt  = require('jsonwebtoken')
require('dotenv').config()

function authMiddleware(req,res,next){
    
    var token = req.cookies.access_token;

    if(token){
        jwt.verify(token, process.env.SECRET_TOKEN,(err,decoded) =>{
            if(err){
                res.status(403).send({ message:"Token expired" });
            }
            req.token_user = decoded.id;
            console.log(req.decoded)
            next()
        })
    }
    else{
        res.status(403).send({ message:"T'as pas de token" });
    }
}

module.exports = authMiddleware;