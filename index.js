var express = require('express')
var http = require('http')
var router = require('./router')
var bodyParser = require('body-parser')
var cookieParser = require('cookie-parser')
var authMiddleware = require('./middlewares/authMiddleware')
require('dotenv').config()

var app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());
// app.use(authMiddleware);

app.use('/',router);


httpServer = http.createServer(app);

httpServer.listen(3000)