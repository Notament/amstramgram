var express = require('express');
var AuthentificationController = require('./controllers/authentificationController');
var PostsController = require('./controllers/postsController');
var UserController = require('./controllers/userController');
var authMiddleware = require('./middlewares/authMiddleware')

var router = express.Router();

router.get("/",authMiddleware, PostsController.getAll);
router.post("/auth/register", AuthentificationController.register);
router.post("/auth/login", AuthentificationController.login);
router.post("/auth/logout", authMiddleware, AuthentificationController.logout);

router.get("/user/:id",authMiddleware, UserController.getById);

router.get("/posts", authMiddleware,PostsController.getAll);
router.get("/posts/:id", authMiddleware,PostsController.getById);
router.post("/posts",authMiddleware, PostsController.newOne);
router.patch("/posts/:id", authMiddleware, PostsController.update);
router.delete("/posts/:id",authMiddleware, PostsController.delete);

module.exports = router;