'use strict';
module.exports = (sequelize, DataTypes) => {
  const Posts = sequelize.define('Posts', {
    title: DataTypes.STRING,
    img: DataTypes.STRING,
    description: DataTypes.STRING,
    userId: DataTypes.INTEGER
  }, {});
  Posts.associate = function(models) {
    // associations can be defined here
    models.Posts.belongsTo(models.User, {
      foreignKey: {
        allowNull: false
      }
    })
  };
  return Posts;
};