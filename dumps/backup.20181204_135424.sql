-- MySQL dump 10.13  Distrib 8.0.13, for Linux (x86_64)
--
-- Host: localhost    Database: instagram_development
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Posts`
--

DROP TABLE IF EXISTS `Posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `Posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `userId` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  CONSTRAINT `Posts_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `Users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Posts`
--

LOCK TABLES `Posts` WRITE;
/*!40000 ALTER TABLE `Posts` DISABLE KEYS */;
INSERT INTO `Posts` VALUES (16,'Hifmoz cewiw noto gela atoavo miem mumetor diti cuwiri va nedavo re safhe.','https://picsum.photos/531','Git cosoraol re luplishi uzejurpeh dazribek ifaze we wockewjom icicuv lajer zukmoste eh deku.',331,'2018-12-04 13:19:22','2018-12-04 13:19:22'),(17,'Ovzub edal tume mifditem bawib ner tikiji kicu pud haj azi gorci fipes heihus juwu ziunu.','https://picsum.photos/532','Zonorozi fezudcet na jesuksor asipu pom ricoz rifjocfi abope ofomafe jekcu litnik ehojo medpedwi wito tosgu.',332,'2018-12-04 13:19:22','2018-12-04 13:19:22'),(18,'Cev ho mig duco zuzamikis nirzow atameful zosfor urkiw jo keos hizeb kejala fihtem nesuk.','https://picsum.photos/533','Luwe idi gu focidife pusbav del gagokefa raenfe veko faf piorowod wokli no icelopet hofli lat.',333,'2018-12-04 13:19:22','2018-12-04 13:19:22'),(19,'Men ovajajeg zenta isgon cejru defcim ju gor voke dottew pijawmi omri awtoivi resafu wesiga.','https://picsum.photos/534','Nam pe ihje vefulomi weeh zup di emazi gekog coc vuf vusjo bunzepriw tosnenec afeb.',334,'2018-12-04 13:19:22','2018-12-04 13:19:22'),(20,'Adre pakvi vofolor adopzor awohifto owaife tezcordem wudpoc zen telo hoasariw tu.','https://picsum.photos/535','Viso hiracto mehan ajfag go efeer gege vote sed ijep mejki iveso foguh la kunuchag.',335,'2018-12-04 13:19:22','2018-12-04 13:19:22'),(21,'Zinip he azwu haesmi tomahe cooce takodu mijjime ikipiwpar bizoh car egihesre vip tohfo.','https://picsum.photos/536','Uji ferefat domob wiucipot lu rosisjo natev vi libfig wigpeop cakogenem waov tejva zisho en.',336,'2018-12-04 13:19:22','2018-12-04 13:19:22'),(22,'Oke sagkil nivvimoj zitbow egiabu bovewiheb nat gi cabis nigmav mifdep zimjon idete.','https://picsum.photos/537','Cepod dejso notwo ci nifwus pinmi jikik teide rowen sehnihec uzdias judditew vifotce pizhuh hos.',337,'2018-12-04 13:19:22','2018-12-04 13:19:22'),(23,'Cizih otlifo irkun zadje et onir di op raf leap vi mif iciviopo kedomige pefeti.','https://picsum.photos/538','Leg zuj lika at gausi boco cunsamme pigid carwe fipoconec ejza bojji iruhu voowe gorzis.',338,'2018-12-04 13:19:22','2018-12-04 13:19:22'),(24,'Du riko dohnu wossepneg fighok koc lo daketiva mihip mazipik mopzifij puwmo ug niiw.','https://picsum.photos/539','Otimin nu ulfoloho aji ok bo otla ic ijera ukahekel moarmi zip mir gevi mih.',339,'2018-12-04 13:19:22','2018-12-04 13:19:22'),(25,'Seovojow vi ine hiuva kobtimdih sisaw irrus cogod obtirpa wekecki epuris ceac.','https://picsum.photos/540','Genol gafvitug tu kavaru hok ga odo cetsip fecawlud temoz kotgavo zuberu piol folpit.',340,'2018-12-04 13:19:22','2018-12-04 13:19:22'),(26,'Pej tovas uwihi ritebeci seddid mam gaja ho et cogpaif juso sigtoji sen tonjo if.','https://picsum.photos/541','Ojaj nafom do josem loiwepiw ibwow erkol zoti haamepe solre mopzi tiribvo dablem.',341,'2018-12-04 13:19:22','2018-12-04 13:19:22'),(27,'Ce zauk ociva obowdek wokiba zoweje muksur arko tujfor rig tir bemu ohi gotomem gevmos paseho sudozi.','https://picsum.photos/542','Ev zo siag nuhpe ihejebfet vemomcew fimri irmi ommak nomwe visar okaguvdik imoziw olwil idotengi.',342,'2018-12-04 13:19:22','2018-12-04 13:19:22'),(28,'Jovo odinuc eci sogsifher tiw mojged nejfom kogiszaf zati timot viwson biwvugbo oseanego cam.','https://picsum.photos/543','Fobu vof kowup foppeh lipnek coficog kuwicuc at nerokoriv fabilaw rejowuzic fotbo ilalopset kirez genu zew.',343,'2018-12-04 13:19:22','2018-12-04 13:19:22'),(29,'Gizzir zape ec gokrerku bipob ruv cicada ron rocwik nimalu pot joca zegijito juce.','https://picsum.photos/544','Aceti solidtar higvig muj eta vikco ewo jel kini ma nidficod dopgu fi olfi kiwofiw zecte.',344,'2018-12-04 13:19:22','2018-12-04 13:19:22'),(30,'Rissoj vove dihohloj avoh to dinzin nido ovzem ibdom er rufpid numvede awuwe vowihmej.','https://picsum.photos/545','Bobfez ne orva fodi moic her uhje opama sekmag hivjomjic ac uwa.',345,'2018-12-04 13:19:22','2018-12-04 13:19:22');
/*!40000 ALTER TABLE `Posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SequelizeMeta`
--

DROP TABLE IF EXISTS `SequelizeMeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `SequelizeMeta` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SequelizeMeta`
--

LOCK TABLES `SequelizeMeta` WRITE;
/*!40000 ALTER TABLE `SequelizeMeta` DISABLE KEYS */;
INSERT INTO `SequelizeMeta` VALUES ('20181203143731-create-user.js'),('20181203143949-create-posts.js');
/*!40000 ALTER TABLE `SequelizeMeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `Users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(255) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=346 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES (331,'John Doe0','john.doe.0@yopmail.com','$2b$12$UkVRfOsPOH51eKLnNafNO.TYryqWgfYVdR0kc609rXfHtyB8A45F6','2018-12-04 13:19:21','2018-12-04 13:19:21'),(332,'John Doe1','john.doe.1@yopmail.com','$2b$12$UkVRfOsPOH51eKLnNafNO.TYryqWgfYVdR0kc609rXfHtyB8A45F6','2018-12-04 13:19:21','2018-12-04 13:19:21'),(333,'John Doe2','john.doe.2@yopmail.com','$2b$12$UkVRfOsPOH51eKLnNafNO.TYryqWgfYVdR0kc609rXfHtyB8A45F6','2018-12-04 13:19:21','2018-12-04 13:19:21'),(334,'John Doe3','john.doe.3@yopmail.com','$2b$12$UkVRfOsPOH51eKLnNafNO.TYryqWgfYVdR0kc609rXfHtyB8A45F6','2018-12-04 13:19:21','2018-12-04 13:19:21'),(335,'John Doe4','john.doe.4@yopmail.com','$2b$12$UkVRfOsPOH51eKLnNafNO.TYryqWgfYVdR0kc609rXfHtyB8A45F6','2018-12-04 13:19:21','2018-12-04 13:19:21'),(336,'John Doe5','john.doe.5@yopmail.com','$2b$12$UkVRfOsPOH51eKLnNafNO.TYryqWgfYVdR0kc609rXfHtyB8A45F6','2018-12-04 13:19:21','2018-12-04 13:19:21'),(337,'John Doe6','john.doe.6@yopmail.com','$2b$12$UkVRfOsPOH51eKLnNafNO.TYryqWgfYVdR0kc609rXfHtyB8A45F6','2018-12-04 13:19:21','2018-12-04 13:19:21'),(338,'John Doe7','john.doe.7@yopmail.com','$2b$12$UkVRfOsPOH51eKLnNafNO.TYryqWgfYVdR0kc609rXfHtyB8A45F6','2018-12-04 13:19:21','2018-12-04 13:19:21'),(339,'John Doe8','john.doe.8@yopmail.com','$2b$12$UkVRfOsPOH51eKLnNafNO.TYryqWgfYVdR0kc609rXfHtyB8A45F6','2018-12-04 13:19:21','2018-12-04 13:19:21'),(340,'John Doe9','john.doe.9@yopmail.com','$2b$12$UkVRfOsPOH51eKLnNafNO.TYryqWgfYVdR0kc609rXfHtyB8A45F6','2018-12-04 13:19:21','2018-12-04 13:19:21'),(341,'John Doe10','john.doe.10@yopmail.com','$2b$12$UkVRfOsPOH51eKLnNafNO.TYryqWgfYVdR0kc609rXfHtyB8A45F6','2018-12-04 13:19:21','2018-12-04 13:19:21'),(342,'John Doe11','john.doe.11@yopmail.com','$2b$12$UkVRfOsPOH51eKLnNafNO.TYryqWgfYVdR0kc609rXfHtyB8A45F6','2018-12-04 13:19:21','2018-12-04 13:19:21'),(343,'John Doe12','john.doe.12@yopmail.com','$2b$12$UkVRfOsPOH51eKLnNafNO.TYryqWgfYVdR0kc609rXfHtyB8A45F6','2018-12-04 13:19:21','2018-12-04 13:19:21'),(344,'John Doe13','john.doe.13@yopmail.com','$2b$12$UkVRfOsPOH51eKLnNafNO.TYryqWgfYVdR0kc609rXfHtyB8A45F6','2018-12-04 13:19:21','2018-12-04 13:19:21'),(345,'John Doe14','john.doe.14@yopmail.com','$2b$12$UkVRfOsPOH51eKLnNafNO.TYryqWgfYVdR0kc609rXfHtyB8A45F6','2018-12-04 13:19:21','2018-12-04 13:19:21');
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-04 13:54:28
